package com.tinywind.boot;

import com.tinywind.boot.common.utils.JsonTool;
import com.tinywind.boot.module.system.service.LoginService;
import com.tinywind.boot.module.system.vo.MenuVo;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class BootApplicationTests {
    final static Logger logger = LoggerFactory.getLogger(BootApplicationTests.class);

    @Test
    void contextLoads() {
    }


    @Autowired
    LoginService loginService;

    @Test
    public void jdbcTest() {
        // 查询List列表

        List<MenuVo> menuList = loginService.listMenuByUserId("1");

        logger.info("{}", JsonTool.toJsonString(menuList));
    }

}
