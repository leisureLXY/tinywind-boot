package com.tinywind.boot.common.config;


import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuxingyu01
 * @date 2021-12-22-21:08
 **/
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {


    /**
     * 自定义拦截器，添加拦截路径和排除拦截路径
     * addPathPatterns(): 添加需要拦截的路径
     * excludePathPatterns(): 添加不需要拦截的路径
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 注册登录拦截器
        registry.addInterceptor(new SaRouteInterceptor((req, resp, handler) -> {
            List<String> excludePathList = new ArrayList<>();
            excludePathList.add("/system/login"); // 登陆接口
            excludePathList.add("/system/getCaptcha"); // 获取图形验证码接口
            excludePathList.add("/anon/**");  // 开放itfc和anon接口（itfc和anon开头的可免认证访问）
            excludePathList.add("/itfc/**");  // 开放itfc和anon接口（itfc和anon开头的可免认证访问）
            excludePathList.add("/static/**");//静态资源不拦截

            // 开放静态资源
            excludePathList.add("/css/**");
            excludePathList.add("/images/**");
            excludePathList.add("/js/**");
            excludePathList.add("/lib/**");
            excludePathList.add("/api/**");

            // swager文档
            excludePathList.add("/swagger-resources/**");
            excludePathList.add("/webjars/**");
            excludePathList.add("/v2/**");
            excludePathList.add("/swagger-ui.html");
            excludePathList.add("/doc.html");
            excludePathList.add("/service-worker.js");

            // 除白名单路径外均需要登录认证
            SaRouter.match("/**").notMatch(excludePathList).check(r -> StpUtil.checkLogin());
        })).addPathPatterns("/**");

        // 注册注解拦截器，并排除不需要注解鉴权的接口地址 (与登录拦截器无关)
        registry.addInterceptor(new SaAnnotationInterceptor())
                // 需要拦截的路径
                .addPathPatterns("/**");

    }

}
