package com.tinywind.boot.common.config.security;

import cn.dev33.satoken.listener.SaTokenListener;
import cn.dev33.satoken.stp.SaLoginModel;
import com.tinywind.boot.common.utils.web.ServletUtils;
import com.tinywind.boot.module.system.service.LoginService;
import com.tinywind.boot.module.system.service.SysLoginLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuxingyu01
 * @date 2022-03-23 22:20
 * @description
 **/
@Component
public class SaTokenListenerImpl implements SaTokenListener {
    final static Logger logger = LoggerFactory.getLogger(SaTokenListenerImpl.class);


    @Autowired
    private SysLoginLogService sysLoginLogService;

    /**
     * 每次登录成功时触发
     */
    @Override
    public void doLogin(String loginType, Object loginId, SaLoginModel loginModel) {
        logger.info("SaTokenListenerImpl - doLogin - loginId = {}", loginId);
        // 保存登录日志
        // sysLoginLogService.saveLoginlog(ServletUtils.getRequest(), );
    }

    /**
     * 每次注销时触发
     */
    @Override
    public void doLogout(String loginType, Object loginId, String tokenValue) {
        // 保存登录日志
        // sysLoginLogService.saveLoginlog(loginService.getUserInfo(Integer.parseInt(String.valueOf(loginId))).getAccount(), 0, LoginEnums.LOGOUT_SUCCESS.getMsg(), ServletUtils.getRequest());
    }

    /**
     * 每次被踢下线时触发
     */
    @Override
    public void doKickout(String loginType, Object loginId, String tokenValue) {
        // 保存登录日志
        // sysLoginLogService.saveLoginlog(loginService.getUserInfo(Integer.parseInt(String.valueOf(loginId))).getAccount(), 0, LoginEnums.FORCE_OFFLINE.getMsg(), ServletUtils.getRequest());
    }

    /**
     * 每次被顶下线时触发
     */
    @Override
    public void doReplaced(String loginType, Object loginId, String tokenValue) {
        // 保存登录日志
        // sysLoginLogService.saveLoginlog();
    }

    /**
     * 每次被封禁时触发
     */
    @Override
    public void doDisable(String loginType, Object loginId, long disableTime) {
        // 保存登录日志
        // sysLoginLogService.saveLoginlog();
    }

    /**
     * 每次被解封时触发
     */
    @Override
    public void doUntieDisable(String loginType, Object loginId) {
        // 保存登录日志
        // sysLoginLogService.saveLoginlog(loginService.getUserInfo(Integer.parseInt(String.valueOf(loginId))).getAccount(), 0, LoginEnums.UNSEAL_LOGIN.getMsg(), ServletUtils.getRequest());
    }

    /**
     * 每次创建Session时触发
     */
    @Override
    public void doCreateSession(String id) {
        // ...
    }

    /**
     * 每次注销Session时触发
     */
    @Override
    public void doLogoutSession(String id) {
        // ...
    }

}
