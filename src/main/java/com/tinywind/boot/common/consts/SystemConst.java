package com.tinywind.boot.common.consts;

/**
 * @author liuxingyu01
 * @date 2021-12-22-22:03
 * @description 系统常量类
 **/
public final class SystemConst {
    /**
     * 系统名称
     */
    public static final String SYSTEM_NAME = "tinywind-boot";

    /**
     * 登录用户信息
     */
    public static final String SYSTEM_USER_INFO = "tinywind_user_info";

    /**
     * 用户菜单
     */
    public static final String SYSTEM_USER_MENU = "tinywind_user_menu";

    /**
     * 用户角色
     */
    public static final String SYSTEM_USER_ROLE = "tinywind_user_role";

    /**
     * 用户权限
     */
    public static final String SYSTEM_USER_PERMISSION = "tinywind_user_permission";

    /**
     * 用户登陆次数
     */
    public static final String SYSTEM_LOGIN_TIMES = "tinywind:logintimes";

    /**
     * itfc-key
     */
    public static final String SYSTEM_ITFC_KEY = "tinywind:itfckey";

    /**
     * id-table业务流水号
     */
    public static final String SYSTEM_ID_TABLE = "tinywind:idtable";

    /**
     * 请求频率限制
     */
    public static final String SYSTEM_REQ_LIMIT = "tinywind:reqlimit";

    /**
     * 系统配置信息
     */
    public static final String SYS_CONFIGURE = "tinywind:configure";

    /**
     * 服务监控信息
     */
    public static final String SYS_SERVER_INFO = "tinywind:serverinfo";

    /**
     * 字典信息
     */
    public static final String SYS_DICT = "tinywind:dict";


    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "tinywind:captcha_codes";

}
