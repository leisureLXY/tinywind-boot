package com.tinywind.boot.common.enums;

/**
 * @author liuxingyu01
 * @date 2021-12-24-12:56
 * @description 数据库类型枚举
 **/
public enum DbType {
    MySQL,
    Oracle,
    DB2,
    H2,
    PostgreSQL,
    HSQLDB,
    Mariadb,
    Sqlite,
    Informix,
    GreenPlum,
    Sqlserver,
    Sqlserver2012,
    Derby,
    UnKown
}
