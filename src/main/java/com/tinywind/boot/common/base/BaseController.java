package com.tinywind.boot.common.base;

import cn.dev33.satoken.stp.StpUtil;
import com.tinywind.boot.common.consts.SystemConst;
import com.tinywind.boot.module.system.entity.SysUserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:03
 **/
public abstract class BaseController {
    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

    public static final String RESULT_ROWS = "rows";

    public static final String RESULT_TOTAL = "total";

    public BaseController() {
    }


    /**
     * 获取登录用户
     *
     * @return
     */
    public SysUserInfo getSysUserInfo() {
        return StpUtil.getTokenSession().getModel(SystemConst.SYSTEM_USER_INFO, SysUserInfo.class);
    }

    /**
     * 获取登录用户ID
     *
     * @return
     */
    public String getSysUserId() {
        return getSysUserInfo().getUserId();
    }

    /**
     * 获取登录用户账户account
     *
     * @return
     */
    public String getSysUserAccount() {
        return getSysUserInfo().getAccount();
    }

    /**
     * 获取用户token
     * @return
     */
    public String getUserKey() {
        return StpUtil.getTokenValue();
    }
}
