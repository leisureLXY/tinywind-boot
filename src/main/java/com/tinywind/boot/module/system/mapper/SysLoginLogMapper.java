package com.tinywind.boot.module.system.mapper;

import com.tinywind.boot.module.system.entity.SysLoginLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author liuxingyu01
 * @date 2021-12-25-21:50
 **/
@Repository
public interface SysLoginLogMapper {
    int saveLoginlog(SysLoginLog sysLoginLog);

    List<SysLoginLog> getLoginLoglist(Map map);

    List<Map> onlineList(Map paraMap);

    int batchDelete(@Param("logIdList") List<String> logIdList);

    void emptyLog();
}
