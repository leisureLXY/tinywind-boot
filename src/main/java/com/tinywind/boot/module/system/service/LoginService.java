package com.tinywind.boot.module.system.service;

import com.tinywind.boot.module.system.entity.SysUserInfo;
import com.tinywind.boot.module.system.vo.MenuVo;

import java.util.List;


/**
 * @author liuxingyu01
 * @date 2021-12-23-12:52
 **/
public interface LoginService {
    SysUserInfo getUserInfo(String account);

    List<MenuVo> listMenuByUserId(String userId);
}
