package com.tinywind.boot.module.system.service.impl;

import com.tinywind.boot.module.system.mapper.SysRolePermissionMapper;
import com.tinywind.boot.module.system.service.SysRolePermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:30
 **/
@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService {

    final static Logger logger = LoggerFactory.getLogger(SysRolePermissionServiceImpl.class);

    @Autowired
    private SysRolePermissionMapper permissionMapper;


    @Override
    public Set<String> listRolePermissionByUserId(String userId) {
        Set<String> set = permissionMapper.listRolePermissionByUserId(userId);
        return set;
    }


}
