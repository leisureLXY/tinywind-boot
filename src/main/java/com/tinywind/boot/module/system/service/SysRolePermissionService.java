package com.tinywind.boot.module.system.service;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:25
 **/
public interface SysRolePermissionService {

    /**
     * 登录时，根据用户id查询所有的权限标识
     */
    Set<String> listRolePermissionByUserId(String userId);
}
