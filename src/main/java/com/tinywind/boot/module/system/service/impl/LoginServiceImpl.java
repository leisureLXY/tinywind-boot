package com.tinywind.boot.module.system.service.impl;

import com.tinywind.boot.module.system.entity.SysUserInfo;
import com.tinywind.boot.module.system.mapper.LoginMapper;
import com.tinywind.boot.module.system.service.LoginService;
import com.tinywind.boot.module.system.utils.MenuTreeUtil;
import com.tinywind.boot.module.system.vo.MenuVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author liuxingyu01
 * @date 2021-12-22-20:47
 **/
@Service
public class LoginServiceImpl implements LoginService {
    final static Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private LoginMapper loginMapper;

    @Override
    public SysUserInfo getUserInfo(String account) {
        return loginMapper.getUserInfo(account);
    }

    @Override
    public List<MenuVo> listMenuByUserId(String userId) {
        if (logger.isInfoEnabled()) {
            logger.info("LoginServiceImpl -- listMenuByUserId -- start");
        }
        List<MenuVo> menuList = loginMapper.listMenuByUserId(userId);

        if (menuList != null && !menuList.isEmpty()) {
            return MenuTreeUtil.toTree(menuList, "0");
        } else {
            return new ArrayList<MenuVo>();
        }
    }

}


