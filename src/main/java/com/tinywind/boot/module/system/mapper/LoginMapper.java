package com.tinywind.boot.module.system.mapper;

import com.tinywind.boot.module.system.entity.SysUserInfo;
import com.tinywind.boot.module.system.vo.MenuVo;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author liuxingyu01
 * @date 2021-12-22-20:47
 **/
@Repository
public interface LoginMapper {

    SysUserInfo getUserInfo(String account);

    List<MenuVo> listMenuByUserId(String userId);

}
