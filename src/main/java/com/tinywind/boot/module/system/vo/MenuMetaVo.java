package com.tinywind.boot.module.system.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author liuxingyu01
 * @date 2021-12-26-20:05
 * @description @JsonInclude(JsonInclude.Include.NON_NULL)标记是jackson包提供的json序列化方法，
 *              已经集成于Springboot2.0中，此方法的配置意在可以对实体json序列化的时候进行对应的数值处理。
 *              使用前：{"username":"admin", "password":"admin123", "token":null}
 *              使用后：{"username":"admin", "password":"admin123"}
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuMetaVo implements Serializable {
    private static final long serialVersionUID = 5518101080905698238L;

    private String title;

    private String icon;

    private Boolean noCache;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getNoCache() {
        return noCache;
    }

    public void setNoCache(Boolean noCache) {
        this.noCache = noCache;
    }
}
