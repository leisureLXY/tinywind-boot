package com.tinywind.boot.module.system.service;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:24
 **/
public interface SysUserRoleService {
    /**
     * 登录时，根据用户id查询全部角色标识
     */
    Set<String> listUserRoleByUserId(String userId);
}
