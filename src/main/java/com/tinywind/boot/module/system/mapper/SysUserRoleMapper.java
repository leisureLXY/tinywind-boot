package com.tinywind.boot.module.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:27
 **/
@Repository
public interface SysUserRoleMapper {
    /**
     * 登录时，根据用户id查询全部角色标识
     */
    Set<String> listUserRoleByUserId(@Param("userId") String userId);
}
