package com.tinywind.boot.module.system.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tinywind.boot.common.base.BaseController;
import com.tinywind.boot.common.base.BaseResult;
import com.tinywind.boot.module.system.entity.SysLoginLog;
import com.tinywind.boot.module.system.service.SysLoginLogService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author liuxingyu01
 * @date 2021-12-25-21:53
 **/
@RestController
@RequestMapping("/system/loginlog")
public class SysLoginLogController extends BaseController {

    final static Logger logger = LoggerFactory.getLogger(SysLoginLogController.class);

    // 基于构造函数注入
    private final SysLoginLogService sysLoginLogService;
    public SysLoginLogController(SysLoginLogService sysLoginLogService) {
        this.sysLoginLogService = sysLoginLogService;
    }


    /**
     * 登陆日志页面分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "登陆日志页面分页查询", notes = "登陆日志页面分页查询")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public BaseResult list(@RequestParam("pageNum") Integer pageNum,
                           @RequestParam("pageSize") Integer pageSize,
                           @RequestParam(required = false, defaultValue = "", value = "account") String account,
                           @RequestParam(required = false, defaultValue = "", value = "status") String status,
                           @RequestParam(required = false, defaultValue = "", value = "createTime") String createTime,
                           @RequestParam(required = false, defaultValue = "", value = "sortName") String sortName,
                           @RequestParam(required = false, defaultValue = "", value = "sortOrder") String sortOrder) {
        //分页查询
        PageHelper.startPage(pageNum, pageSize);
        if (logger.isInfoEnabled()) {
            logger.info("SysLoginLogController -- list -- 页面大小："+pageSize+"--页码:" + pageNum);
        }

        Map<String, String> paraMap = new HashMap<>();
        paraMap.put("account", account);
        paraMap.put("status", status);
        paraMap.put("sortName", sortName);
        paraMap.put("sortOrder", sortOrder);
        paraMap.put("createTime", createTime);

        List<SysLoginLog> accounts = sysLoginLogService.list(paraMap);

        PageInfo<SysLoginLog> pageinfo = new PageInfo<>(accounts);
        //取出查询结果
        List<SysLoginLog> rows = pageinfo.getList();
        int total = (int) pageinfo.getTotal();
        Map<String, Object> result = new HashMap<>();
        result.put(RESULT_ROWS, rows);
        result.put(RESULT_TOTAL, total);

        return BaseResult.success(result);
    }

}
