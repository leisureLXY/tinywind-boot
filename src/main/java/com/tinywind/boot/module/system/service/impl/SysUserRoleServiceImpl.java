package com.tinywind.boot.module.system.service.impl;

import com.tinywind.boot.module.system.mapper.SysUserRoleMapper;
import com.tinywind.boot.module.system.service.SysUserRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:26
 **/
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

    final static Logger logger = LoggerFactory.getLogger(SysUserRoleServiceImpl.class);

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public Set<String> listUserRoleByUserId(String userId) {
        Set<String> set = sysUserRoleMapper.listUserRoleByUserId(userId);
        return set;
    }

}
