package com.tinywind.boot.module.system.utils;


import com.tinywind.boot.module.system.vo.MenuMetaVo;
import com.tinywind.boot.module.system.vo.MenuVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuxingyu01
 * @date 2021-12-26-20:38
 **/
public class MenuTreeUtil {

    /**
     * 获取菜单树(list转tree)
     *
     * @param treeList
     * @param pid
     * @return
     */
    public static List<MenuVo> toTree(List<MenuVo> treeList, String pid) {
        List<MenuVo> retList = new ArrayList<>();

        for (MenuVo parent : treeList) {

            // 父级是0，类型是菜单，并且没有子级的情况，需要单独处理
            if (pid.equals(parent.getParentId()) && "1".equals(parent.getType())
                    && !hasChildren(treeList, parent.getPermissionId())) {
                MenuVo menuVo1 = new MenuVo();
                if ("0".equals(parent.getIframe())) {
                    menuVo1.setName(parent.getName());
                    menuVo1.setPath(parent.getPath());
                    menuVo1.setComponent(parent.getComponent());
                    menuVo1.setHidden(parent.getHidden());
                } else {
                    menuVo1.setPath(parent.getPath());
                }
                MenuMetaVo meta = new MenuMetaVo();
                meta.setIcon(parent.getIcon());
                meta.setTitle(parent.getTitle());
                meta.setNoCache(parent.getCache());
                menuVo1.setMeta(meta);

                List<MenuVo> list1 = new ArrayList<>();
                list1.add(menuVo1);

                parent.setComponent("Layout");
                parent.setRoot(true);
                parent.setName(null);
                parent.setMeta(null);
                parent.setChildren(list1);

                retList.add(parent);
            } else {
                // 只有parentId为0的情况下，root才为true
                if (pid.equals(parent.getParentId())) {
                    parent.setRoot(true);
                } else {
                    parent.setRoot(false);
                }
                // type = "0"，说明是目录
                if ("0".equals(parent.getType())) {
                    parent.setRedirect("noRedirect");
                    parent.setComponent("Layout");
                    parent.setName(parent.getTitle());
                }

                MenuMetaVo menuMetaVo = new MenuMetaVo();
                menuMetaVo.setIcon(parent.getIcon());
                menuMetaVo.setTitle(parent.getTitle());
                menuMetaVo.setNoCache(parent.getCache());

                parent.setMeta(menuMetaVo);

                if (pid.equals(parent.getParentId())) {
                    retList.add(findChildren(parent, treeList));
                }
            }
        }
        return retList;
    }


    /**
     * 获取子节点
     *
     * @param parent
     * @param treeList
     * @return
     */
    private static MenuVo findChildren(MenuVo parent, List<MenuVo> treeList) {
        for (MenuVo child : treeList) {
            if (parent.getPermissionId().equals(child.getParentId())) {
                if (parent.getChildren() == null) {
                    parent.setChildren(new ArrayList<>());
                }
                parent.getChildren().add(findChildren(child, treeList));
            }
        }
        return parent;
    }


    /**
     * 判断是否有子级
     * @param treeList
     * @param permissionId
     * @return true有子级  false无子级
     */
    private static boolean hasChildren(List<MenuVo> treeList, String permissionId) {
        boolean hasChildren = false;
        for (MenuVo menuVo: treeList) {
            if (menuVo.getParentId().equals(permissionId)) {
                hasChildren= true;
                break;
            }
        }
        return hasChildren;
    }


}
