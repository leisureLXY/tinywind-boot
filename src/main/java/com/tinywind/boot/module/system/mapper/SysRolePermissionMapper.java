package com.tinywind.boot.module.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * @author liuxingyu01
 * @date 2021-12-23-13:28
 **/
@Repository
public interface SysRolePermissionMapper {

    /**
     * 登录时，根据用户id查询所有的权限标识(资源值)
     */
    Set<String> listRolePermissionByUserId(@Param("userId") String userId);


}
